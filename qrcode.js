(function($) {
    Drupal.behaviors.qrcode = {
        attach: function(context, settings) {
            var option = {
                // render method: 'canvas', 'image' or 'div'
                render: 'canvas',
                // version range somewhere in 1 .. 40
                minVersion: 1,
                maxVersion: 40,
                // error correction level: 'L', 'M', 'Q' or 'H'
                ecLevel: 'H',
                // offset in pixel if drawn onto existing canvas
                left: 0,
                top: 0,
                // size in pixel
                size: 160,
                // code color or image element
                fill: null,
                // background color or image element, null for transparent background
                background: null,
                // content
                text: window.location.href,
                // corner radius relative to module width: 0.0 .. 0.5
                radius: 0,
                // quiet zone in modules
                quiet: 0,
                // modes
                // 0: normal
                // 1: label strip
                // 2: label box
                // 3: image strip
                // 4: image box
                mode: 4,
                mSize: 0.3,
                mPosX: 0.5,
                mPosY: 0.5,
                label: 'no label',
                fontname: 'sans',
                fontcolor: '#000',
                image: $("#qrcode-logo")[0]
            }
            $("#qrcode-output").qrcode(option);
        }
    }
})(jQuery);